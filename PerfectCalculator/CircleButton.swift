//
//  CircleButton.swift
//  PerfectCalculator
//
//  Created by songkesong on 2022/7/6.
//

import SwiftUI

//取得手机屏幕宽度
let scWidth: CGFloat = UIScreen.main.bounds.width

struct CircleButton: View {
    
    @State var text: String
    @State var showButtonPressed = false
    @State var acbuttontouched = false
    
    let bstrings = ["÷","x","-","+","="]
    let acstrings = ["AC","+/-","%"]
    let dstrings = [".","0","1","2","3","4","5","6","7","8","9"]
    
    var action: (String) -> ()
    
    func getColor()-> (Color,Color) {
        
        if bstrings.contains(text){
            return showButtonPressed ?  (.orange,.white) : (.white,.orange)
        }
        if acstrings.contains(text){
            return (.black,.white.opacity(showButtonPressed ? 0.9 : 0.6))
        }
        if dstrings.contains(text){
            return (.white,.white.opacity(showButtonPressed ? 0.5 : 0.2))
        }
        return (.clear,.clear)
    }
    
    var body: some View {
        let fbColor : (Color,Color) = getColor()
        
        let diam = (scWidth - 60)/4
        //如果添加GeometryReader 会导致 Grid中的行间距变大
        //GeometryReader { proxy in
        Button {
            action(text)
            
            if text == "AC"{
                if !acbuttontouched {
                    acbuttontouched = true
                }else{
                    acbuttontouched = false
                }
            }
            
            withAnimation(Animation.easeOut(duration: 0.5)) {
                showButtonPressed = true
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                withAnimation(Animation.easeOut(duration: 0.5)) {
                    showButtonPressed = false
                }
            }
        } label: {
            Text(acbuttontouched ? "C" : text)
                .font(Font.custom("苹方-简", size:  diam/2)
                    .weight(.light))
                .frame(maxWidth: .infinity, maxHeight: diam)
                .offset(x:(text=="0") ? -(diam/2 + 5) : 0 )
        }
        .foregroundColor(fbColor.0)
        .background(fbColor.1)
        .clipShape(Rectangle())
        .cornerRadius(diam/2)
    }
}

struct CircleButton_Previews: PreviewProvider {
    static var previews: some View {
        CircleButton(text: "") {_ in
            print("8")
        }
        
    }
}

