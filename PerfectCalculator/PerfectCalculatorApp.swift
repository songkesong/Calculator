//
//  PerfectCalculatorApp.swift
//  PerfectCalculator
//
//  Created by songkesong on 2022/7/6.
//

import SwiftUI

@main
struct PerfectCalculatorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(calculatorControlor: ViewController())
        }
    }
}
