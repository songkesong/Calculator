//
//  ContentView.swift
//  PerfectCalculator
//
//  Created by songkesong on 2022/7/6.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var calculatorControlor: ViewController
    
    var body: some View {
        VStack(alignment: .leading,spacing: 20){
            Spacer()
            Text(calculatorControlor.displaytext!)
                .font(Font.custom("苹方-简", size: 65).weight(.light))
                .frame(maxWidth: .infinity, alignment: .trailing)
                .lineLimit(1)
                .minimumScaleFactor(0.8)
                .foregroundColor(.white)
               
            Grid{
                GridRow{
                    CircleButton(text: "AC") { _ in calculatorControlor.touchACbutton("AC")
                    }
                    CircleButton(text: "+/-") { _ in calculatorControlor.performOperation("+/-")
                    }
                    CircleButton(text: "%") { _ in calculatorControlor.performOperation("%")
                    }
                    CircleButton(text: "÷") { _ in calculatorControlor.performOperation("÷")
                    }
                }
                GridRow{
                    CircleButton(text: "7") { _ in calculatorControlor.touchDigit("7")
                    }
                    CircleButton(text: "8") { _ in calculatorControlor.touchDigit("8")
                    }
                    CircleButton(text: "9") { _ in calculatorControlor.touchDigit("9")
                    }
                    CircleButton(text: "x") { _ in calculatorControlor.performOperation("x")
                    }
                }
                GridRow{
                    CircleButton(text: "4") { _ in calculatorControlor.touchDigit("4")
                    }
                    CircleButton(text: "5") { _ in calculatorControlor.touchDigit("5")
                    }
                    CircleButton(text: "6") { _ in calculatorControlor.touchDigit("6")
                    }
                    CircleButton(text: "-") { _ in calculatorControlor.performOperation("-")
                    }
                }
                GridRow{
                    CircleButton(text: "1") { _ in calculatorControlor.touchDigit("1")
                    }
                    CircleButton(text: "2") { _ in calculatorControlor.touchDigit("2")
                    }
                    CircleButton(text: "3") { _ in calculatorControlor.touchDigit("3")
                    }
                    CircleButton(text: "+") { _ in calculatorControlor.performOperation("+")
                    }
                }
                GridRow{
                    CircleButton(text: "0") { _ in calculatorControlor.touchZero("0")
                    }.gridCellColumns(2)
                    CircleButton(text: ".") { _ in calculatorControlor.touchDecimalPoint(".")
                    }
                    CircleButton(text: "=") { _ in calculatorControlor.performOperation("=")
                    }
                }

            }
        }
        .padding(20)
        .background(.black)
        //设置顶部时间、信号、WIFI电池显示为白色
        .preferredColorScheme(.dark)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(calculatorControlor: ViewController())
    }
}
