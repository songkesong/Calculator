//
//  ViewController.swift
//  CsCalculator
//
//  Created by songkesong on 2022/7/6.
//

import UIKit

class ViewController: ObservableObject {
    
    @Published var displaytext: String? = "0"
    
    var userIsInTheMiddleOfTyping  = false
    
    //ac按钮单独处理
    func touchACbutton(_ buttonText: String){
        if buttonText == "AC" {
            displaytext = "0"
            userIsInTheMiddleOfTyping = false
        }
    }
    
    //判断小数点是否已经输入，如果已经输入，再点击无效
    func touchDecimalPoint(_ buttonText: String) {
        if !String(displaytext!).contains(".") {
            //let digit = (sender.titleLabel?.text)!
            let digit = buttonText
            let textCurrentlyInDisplay = displaytext!
            if userIsInTheMiddleOfTyping {
                displaytext = textCurrentlyInDisplay + digit
            }else{
                //下面一句作用，第一次输入小数点，前面加个0.
                displaytext = textCurrentlyInDisplay + digit
                userIsInTheMiddleOfTyping = true
            }
        }
    }
    //把0点击单独拿出来处理，初次点按0，再按，防止出现00000开头这种数字。
    func touchZero(_ buttonText: String) {
        let digit = buttonText
        if userIsInTheMiddleOfTyping {
            let textCurrentlyInDisplay = displaytext!
            if Double(textCurrentlyInDisplay) == 0.0 && !textCurrentlyInDisplay.contains("."){
                return
            }else{
                displaytext = textCurrentlyInDisplay + digit
            }
        }else{
            displaytext = digit
            userIsInTheMiddleOfTyping = true
        }
    }
    //数字按钮函数
    func touchDigit(_ buttonText: String) {
        let digit = buttonText
        if userIsInTheMiddleOfTyping {
            let textCurrentlyInDisplay = displaytext!
            displaytext = textCurrentlyInDisplay + digit
        }else{
            displaytext = digit
            userIsInTheMiddleOfTyping = true
        }
    }
    
    //计算属性displyValue，负责String-Double互相转化。
    //String 显示用，Double 计算用
    var displayValue: Double {
        get {
            return Double(displaytext!)!
        }
        set {
            displaytext = String(newValue)
        }
    }
    
    private var brain = CalculatorBrain()
    
    //操作按钮响应函数，转到CalculatorBrain里面处理。
    func performOperation(_ buttonText: String) {
        if userIsInTheMiddleOfTyping {
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
        }
        
        if buttonText != "" {
            brain.performOperation(buttonText)
        }
        
        if let result = brain.result {
            displayValue = result
        }
    }
}

