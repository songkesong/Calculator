//
//  CalculatorBrain.swift
//  PerfectCalculator
//
//  Created by songkesong on 2022/7/6.
//

import Foundation

struct CalculatorBrain {
    private var accumulator: Double?
    
    //不只是可选类型有关联值，所有枚举类型也有关联值。
    //Swift中的Optional底层实现是enum，Optional的本质使用一个具有关联值的enum。
    //如constant(Double）后面括号里面的就是关联值
    private enum Operation {
        case constant(Double)
        case unaryOperation((Double)-> Double)
        case binaryOperation((Double,Double)-> Double)
        case equals
    }
    
    private var operations: Dictionary<String,Operation> = [
        "𝜋": Operation.constant(Double.pi),
        "e": Operation.constant(M_E),
        "√": Operation.unaryOperation(sqrt),
        "cos": Operation.unaryOperation(cos),
        "%":Operation.unaryOperation({$0 / 100}),
        "+/-": Operation.unaryOperation({ -$0 }),    //闭包当函数参数用
        "x": Operation.binaryOperation({$0 * $1}), //闭包当函数参数用
        "÷": Operation.binaryOperation({$0 / $1}),
        "+": Operation.binaryOperation({$0 + $1}),
        "-": Operation.binaryOperation({$0 - $1}),
        "=": Operation.equals
    ]
    
    //下面函数中的switch中的let value ，let function 是提取关联值做为一个常量。
    //如果提取为变量，可以写成 var value ， var function
    
    mutating func performOperation(_ symbol: String){
        if let operation = operations[symbol] {
            switch operation {
            case .constant(let value):
                accumulator = value
            case .unaryOperation(let function):
                if accumulator != nil {
                    accumulator = function(accumulator!)
                }
            case .binaryOperation(let function):
                if accumulator != nil {
                    pendingBinaryOperation = PendingBinaryOperation(function: function, firstOperand: accumulator!)
                    accumulator = nil
                }
            case .equals:
                peformPendingBinaryOperation()
            }
        }
    }
    private  mutating func peformPendingBinaryOperation() {
        if pendingBinaryOperation != nil && accumulator != nil {
            accumulator = pendingBinaryOperation!.perform(with: accumulator!)
            pendingBinaryOperation = nil
        }
    }
    
    private var pendingBinaryOperation: PendingBinaryOperation?
    
    //提供一个结构体，存储二元计算所需的一切
    private struct PendingBinaryOperation {
        let function: (Double,Double)-> Double
        let firstOperand: Double
        
        func perform(with secondOperand: Double)-> Double {
            return function(firstOperand,secondOperand)
        }
    }
    
    mutating func setOperand(_ operand: Double) {
        accumulator = operand
    }
    
    var result: Double? {
        get {
            return accumulator
        }
    }
}
